Hello there! Welcome to the C-eye repository!

We are three young researchers, Sergio, Giovanni and Emanuele and we are here to show our idea to fight Covid-19.

Starting by the inspiration, the idea behind our project takes inspiration from one of the possible solutions to face Covid-19, namely respecting a series of non-habitual rules until a vaccine will be released.

So, social distancing is one of the fundamental rules!

….and wearing protective masks!

Now, what is C-eye?

C-eye is a solution based on intelligent computer vision techniques for the detection of non-compliance with anti-covid rules

The streamed video is processed from the artificial intelligence algorithm which detects gatherings, relevant elements (as people faces) and possible violations.

But, the question is: What problems does C-Eye solve?

For business continuity it is necessary that there is compliance with the Covid-19 anticontagion rules.

Who is C-Eye for? We identified to controlling fields, which assume suitable to our idea.

Commercial and service activities: Supermarket, little e medium shop, restaurants and pubs, banks, offices, … and mores

Local government agencies, for control: administrative offices, squares and parks, churches and museums, bus or train, … and mores


**How to use the code**

The code is currently being developed. The neural network needs to be trained on a larger set of images to improve accuracy.

To use the code, we suggest create a virtual environment: go to your project’s directory and run the following command. 

python3 -m venv env

After creating the virtual environment, run the following command to install the packages needed to run the code:

pip install -r requirements.txt

To run the code, run the following command:

python image_analysis.py

The code makes use of a framework for object instance segmentation using convolutional neural networks presented in https://arxiv.org/abs/1703.06870. This code can be found in https://github.com/matterport/Mask_RCNN
