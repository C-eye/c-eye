import os, sys, time
import datetime as dt
import cv2
import tensorflow as tf
import pytz

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
from detect import InferenceConfig, detect_and_color_splash
ROOT_DIR = os.path.abspath("neural_network/")
sys.path.append(ROOT_DIR)
from mrcnn import model as modellib

image_path = 'images/'
image_path_out = 'analyzed_images/'
weights_path = 'weights/' #+ add filename weights training
logs = 'logs/'
conversion_pixel_cm = 2.54/96
tz = pytz.timezone('Europe/Rome')


timeinterval_video = 1  # sampling time in seconds
durata_acquisizione_video = 60 #minutes


def main():

    endtime = dt.datetime.now(tz) + dt.timedelta(minutes=durata_acquisizione_video)  # tempo finale
    cam_devolgitore = cv2.VideoCapture('rtsp://admin:passwd@192.168.10.56:554//h264Preview_01_sub')

    while True:
        if dt.datetime.now(tz) >= endtime:
            break
        else:
            try:
                timestart = dt.datetime.now(tz)
                timestamp = timestart.strftime('%Y%m%d_%H%M%S')
                print('Start capturing..', dt.datetime.now(tz).strftime('%Y-%m-%d %H:%M:%S'))

                ret_1, frame_1 = cam_devolgitore.read()
                str_pic_devolgitore = 'CAM1_' + str(timestamp) + '.jpg'
                cv2.imwrite(image_path + str_pic_devolgitore, frame_1)

                maskface_detection(str_pic_devolgitore)
                time_to_wait = (timestart + dt.timedelta(seconds=timeinterval_video) - dt.datetime.now(tz)).seconds
                print('waiting ' + str(time_to_wait) + 'seconds...')
                time.sleep(time_to_wait)

            finally:
                cam_devolgitore.release()
                print('Stop acquisition')


def maskface_detection(filename):
    config = InferenceConfig()
    #config.display()
    model = modellib.MaskRCNN(mode="inference", config=config, model_dir=logs)
    model.load_weights(weights_path, by_name=True)
    if os.path.exists(image_path + filename):
        image = image_path + filename
        print(image, dt.datetime.now(tz).time().strftime('%H:%M:%S'))
        detect_and_color_splash(model, image_path=image, out_dir=image_path_out)
    else:
        files_name = [i for i in os.listdir(image_path) if os.path.isfile(os.path.join(image_path, i))]# and 'Preview' == i[:7] and 'detect' != i[-10:-4]]
        for file in files_name[::-1]:
            print(file)
            image = image_path + file
            print(image, dt.datetime.now(tz).time().strftime('%H:%M:%S'))
            detect_and_color_splash(model, image_path=image, out_dir=image_path_out)


if __name__ == '__main__':
    #maskface_detection('prov.jpg')
    main()